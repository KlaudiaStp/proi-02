#include "ListofComplexNumbers.h"

ListofComplexNumbers::ListofComplexNumbers()
{
    size=0;
    head=NULL;
    tail=NULL;

}

ListofComplexNumbers::~ListofComplexNumbers()
{
    Element *tmp;
    while(head!=NULL)
    {
    tmp=head->next;
    delete head->z;
    delete head;
    head=tmp;
    }

}
void ListofComplexNumbers::addAtEnd(ComplexNumber *z)
{
    Element *tmp = new Element;
    tmp->z= new ComplexNumber;
    tmp->z->setRe(z->getRe());
    tmp->z->setIm(z->getIm());
    tmp->prev=tail;
    tmp->next=NULL;
    tail=tmp;
    ++size;
    if(tmp->prev!=NULL)
    {
        tmp->prev->next=tmp;
    }
    else
    {
        head=tmp;
    }


}
void ListofComplexNumbers::addAtBeginning (ComplexNumber *z)
{
    Element *tmp = new Element;
    tmp->z= new ComplexNumber;
    tmp->z->setRe(z->getRe());
    tmp->z->setIm(z->getIm());
    tmp->next=head;
    tmp->prev=NULL;
    head=tmp;
    ++size;
    if(tmp->next!=NULL)
    {
        tmp->next->prev=tmp;
    }
    else
    {
        tail=tmp;
    }

}
std::ostream & operator<<(std::ostream &os, ListofComplexNumbers &list)
{

    Element *tmp;
    tmp=list.head;
    for(int i=0; i<list.size; i++)
    {
        os<<*(tmp->z)<<std::endl;
        tmp=tmp->next;
    }
    return os;
}
void ListofComplexNumbers::operator+=(ListofComplexNumbers &l)
{
    Element *tmp;
    tmp=l.head;
    for(int i=0; i<l.size; ++i)
    {
       addAtEnd(tmp->z);
       tmp=tmp->next;
    }


}
ListofComplexNumbers* ListofComplexNumbers::operator+(ListofComplexNumbers &l)
{
    ListofComplexNumbers* l2= new ListofComplexNumbers();
    Element *tmp;
    tmp=head;
    for(int i=0; i<size; ++i)
    {
        l2->addAtEnd(tmp->z);
        tmp=tmp->next;
    }
    tmp=l.head;
    for(int i=0; i<l.size; ++i)
    {
       l2->addAtEnd(tmp->z);
       tmp=tmp->next;
    }

    return l2;
}
ComplexNumber* ListofComplexNumbers::operator[](int k)
{
    if(k>=0 && k<size){
        Element *tmp;
        tmp=head;
        for(int i=0; i<k; ++i)
        {
            tmp=tmp->next;
        }
        return tmp->z;
    }
    else return NULL;
}
