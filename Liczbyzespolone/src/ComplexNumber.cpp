#include "ComplexNumber.h"

ComplexNumber::ComplexNumber()
{
    re=0;
    im=0;
}

ComplexNumber::~ComplexNumber()
{
    //dtor
}

ComplexNumber::ComplexNumber( double vre, double vim)
{
    re=vre;
    im=vim;
}

double ComplexNumber::getRe()
{
    return re;
}

double ComplexNumber::getIm()
{
    return im;
}

void ComplexNumber::setRe( double vre)
{

    re=vre;
}

void ComplexNumber::setIm (double vim)
{
    im=vim;
}


void ComplexNumber::operator+=(ComplexNumber &z)
{
    re+=z.re;
    im+=z.im;
}
void ComplexNumber::operator-=(ComplexNumber &z)
{
    re-=z.re;
    im-=z.im;
}
ComplexNumber* ComplexNumber::operator+(ComplexNumber &z1)
{
    ComplexNumber*z = new ComplexNumber();
    z->re=re+z1.re;
    z->im=im+z1.im;
    return z;
}
ComplexNumber* ComplexNumber::operator-(ComplexNumber &z1)
{
    ComplexNumber*z = new ComplexNumber();
    z->re=re-z1.re;
    z->im=im-z1.im;
    return z;
}
bool ComplexNumber::operator==(ComplexNumber &z)
{
    return re==z.re && im==z.im;

}
bool ComplexNumber::operator!=(ComplexNumber &z)
{
    return re!=z.re && im!=z.im;

}
std::ostream & operator<<(std::ostream & os, ComplexNumber &z)
{
    os<<z.re<< (z.im>=0?"+":"")<<z.im<<"j";
    return os;
}
