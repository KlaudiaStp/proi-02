#ifndef LISTOFCOMPLEXNUMBERS_H
#define LISTOFCOMPLEXNUMBERS_H
#include "ComplexNumber.h"

struct Element {

    ComplexNumber *z;
    Element *next;
    Element *prev;

};
class ListofComplexNumbers
{
    public:
        void addAtEnd (ComplexNumber *z);
        void addAtBeginning (ComplexNumber *z);
        ListofComplexNumbers();
        ListofComplexNumbers * operator+(ListofComplexNumbers &l);
        void operator+=(ListofComplexNumbers &l);
        ComplexNumber* operator[](int k);
        virtual ~ListofComplexNumbers();
        friend std::ostream & operator<<(std::ostream &os, ListofComplexNumbers &list);
    protected:
    private:
        Element *head;
        Element *tail;
        int size;

};

#endif // LISTOFCOMPLEXNUMBERS_H
