#ifndef COMPLEXNUMBER_H
#define COMPLEXNUMBER_H
#include <iostream>


class ComplexNumber
{
public:
    ComplexNumber();
    ComplexNumber( double vre, double vim);
    double getRe();
    double getIm();
    void setRe( double vre);
    void setIm (double vim);
    void operator+=(ComplexNumber &z);
    void operator-=(ComplexNumber &z);
    bool operator==(ComplexNumber &z);
    bool operator!=(ComplexNumber &z);
    ComplexNumber* operator+(ComplexNumber &z1);
    ComplexNumber* operator-(ComplexNumber &z1);
    friend std::ostream & operator<<(std::ostream & os, ComplexNumber &z);
    virtual ~ComplexNumber();
protected:
private:

    double re;
    double im;
};

#endif // COMPLEXNUMBER_H
